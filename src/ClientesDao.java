import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class ClientesDao {
	private Connection conexao;

	public ClientesDao() {
		this.conexao = Conexao.getConnection();
	}
	public void inserir(Clientes cliente) {
		String query = "INSERT INTO clientes (cpf, cnh, nome_cliente, email_cliente, tel1, tel2, data_nascimento, logradouro, num_endereco, cep, bairro, cidade, estado)" + 
				"values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, cliente.getCpf());
			statement.setString(2, cliente.getCnh());
			statement.setString(3, cliente.getNome_cliente());
			statement.setString(4, cliente.getEmail_cliente());
			statement.setString(5, cliente.getTel1());
			statement.setString(6, cliente.getTel2());
			statement.setDate(7, cliente.getData_nascimento());
			statement.setString(8, cliente.getLogradouro());
			statement.setString(9, cliente.getNum_endereco());
			statement.setString(10, cliente.getCep());
			statement.setString(11, cliente.getBairro());
			statement.setString(12, cliente.getCidade());
			statement.setString(13, cliente.getEstado());
			
			statement.execute();
			statement.close();
		} catch  (SQLException e){
			throw new RuntimeException(e);
		}
	}
	
	public List<Clientes> consultar(){
		String query = "SELECT * from clientes";
		List<Clientes> clientes = new ArrayList<Clientes>();
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			
			ResultSet resultado = statement.executeQuery();
			
			while(resultado.next()) {
				Clientes cliente = new Clientes();
				cliente.setCpf(resultado.getString("cpf"));
				cliente.setCnh(resultado.getString("cnh"));
				cliente.setNome_cliente(resultado.getString("nome_cliente"));
				cliente.setEmail_cliente(resultado.getString("email_cliente"));
				cliente.setTel1(resultado.getString("tel1"));
				cliente.setTel2(resultado.getString("tel2"));
				cliente.setData_nascimento(resultado.getDate("data_nascimento"));
				cliente.setLogradouro(resultado.getString("logradouro"));
				cliente.setNum_endereco(resultado.getString("num_endereco"));
				cliente.setCep(resultado.getString("cep"));
				cliente.setBairro(resultado.getString("bairro"));
				cliente.setCidade(resultado.getString("cidade"));
				cliente.setEstado(resultado.getString("estado"));
				
				clientes.add(cliente);
			}
			resultado.close();
			statement.close();
			
			return clientes;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<Clientes> consultarComWhere(String email){
		String query = "SELECT * from clientes WHERE email_cliente like ?";
		List<Clientes> clientes = new ArrayList<Clientes>();
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
//			statement.setString(1, email);
			statement.setString(1, "%" + email);
			
			ResultSet resultado = statement.executeQuery();
			
			while(resultado.next()) {
				Clientes cliente = new Clientes();
				cliente.setCpf(resultado.getString("cpf"));
				cliente.setCnh(resultado.getString("cnh"));
				cliente.setNome_cliente(resultado.getString("nome_cliente"));
				cliente.setEmail_cliente(resultado.getString("email_cliente"));
				cliente.setTel1(resultado.getString("tel1"));
				cliente.setTel2(resultado.getString("tel2"));
				cliente.setData_nascimento(resultado.getDate("data_nascimento"));
				cliente.setLogradouro(resultado.getString("logradouro"));
				cliente.setNum_endereco(resultado.getString("num_endereco"));
				cliente.setCep(resultado.getString("cep"));
				cliente.setBairro(resultado.getString("bairro"));
				cliente.setCidade(resultado.getString("cidade"));
				cliente.setEstado(resultado.getString("estado"));
				
				clientes.add(cliente);
			}
			resultado.close();
			statement.close();
			
			return clientes;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void apagar(String cpf){
		String query = "DELETE from clientes WHERE cpf = ?";
		
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, cpf);
			
			statement.execute();
			statement.close();
			
		} catch  (SQLException e){
			throw new RuntimeException(e);
		}
		
	}
	
	public void alterar(String cpf, String tel1Novo ){
		String query = "UPDATE clientes SET tel1 = ? WHERE cpf = ?";
		
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, tel1Novo);
			statement.setString(2, cpf);
			
			statement.execute();
			statement.close();
			
		} catch  (SQLException e){
			throw new RuntimeException(e);
		}
		
	}
}

