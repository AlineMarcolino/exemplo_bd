import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class Main {
	static ClientesDao dao = new ClientesDao();
	
	public static void main(String[] args) throws SQLException{
		Clientes novoCliente = new Clientes();
		novoCliente.setCpf("2661239801");
		novoCliente.setCnh("344.343-03");
		novoCliente.setNome_cliente("Robsao Cavalcante");
		novoCliente.setEmail_cliente("rob_son@uol.com.br");
		novoCliente.setTel1("11923460998");
		novoCliente.setTel2(null);
		novoCliente.setData_nascimento(new Date(1981, 01, 31));
		novoCliente.setLogradouro("Rua Sei La");
		novoCliente.setNum_endereco("90");
		novoCliente.setCep("09456670");
		novoCliente.setBairro("Bairro do Robson");
		novoCliente.setCidade("Osasco");
		novoCliente.setEstado("SP");

		dao.inserir(novoCliente);
		
		//System.out.println("Cliente inserido com sucesso!");
		
//		List<Clientes> clientes = dao.consultar();
//		
//		for (Clientes cli : clientes) {
//			System.out.println(cli.getNome_cliente() + " | " + cli.getData_nascimento());
//		}
		
		mostrarClientes();
		
		String cpfApagar = "2661239801";
		
		dao.apagar(cpfApagar);
		System.out.println("\nCliente " + cpfApagar + " apagado com sucesso!\n");
		
//		List<Clientes> clientesAtualizados = dao.consultar();
//		for (Clientes cli : clientesAtualizados) {
//			System.out.println(cli.getNome_cliente() + " | " + cli.getData_nascimento());
//		}
		
		mostrarClientes();
		
		String cpfAlterar = "30074799802";
		String tel1Novo = "11981267488";
		
		dao.alterar(cpfAlterar, tel1Novo);
		System.out.println("\nCliente " + cpfAlterar + " alterado com sucesso!\n");
		
		mostrarClientes();
		
		String emailCliente = "@uol.com.br";
		
		List<Clientes> clis = dao.consultarComWhere(emailCliente);
		System.out.println("\n Consulta Email: \n");
		for (Clientes cli : clis) {
			System.out.println(cli.getNome_cliente() + " | " + cli.getData_nascimento() + " | " + cli.getTel1() + " | " + cli.getEmail_cliente());
		}
	}
	
	private static void mostrarClientes() {
		
		List<Clientes> clientes = dao.consultar();
		
		for (Clientes cli : clientes) {
			System.out.println(cli.getNome_cliente() + " | " + cli.getData_nascimento() + " | " + cli.getTel1());
		}
		
	}
}
